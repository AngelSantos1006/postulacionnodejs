# SpringLabs

[![N|Solid](https://media-exp1.licdn.com/dms/image/C510BAQFwuqeqniHBZA/company-logo_200_200/0?e=2159024400&v=beta&t=6NzQE2q0ajsrE0Sqk3wwduccebQ1GltG4_6FCIeVUrY)](https://springlabs.ai)

El siguiente proyecto es una prueba para desarrolladores que aspiran a una vacante dentro de SpringLabs S.A. de C.V. para los siguiente perfiles:

  - Desarrollador Full-Stack(Nodejs, Reacjs)
  - Desarrollador Front-End(Reactjs)
  - Desarrollador Back-End(Nodejs)

# Examen Postulación!

Recomendaciones:
  - Buenas practicas
  - Reducción de problemas
  - Documentación de codigo

> Recuerda que para todo desarrollador es importante 
> realizar buenas practicas, ya que ayudaran a futuros
> desarrolladores a entender y comprender el codigo
> que creamos.


La siguiente evaluación, nos ayudara a evaluar el nivel de conocimiento
aptitudes y logica que tienes para la resolcion de problemas.

### Buena Suerte!!!

Conocimientos previos:

* [ReactJS] - librería JavaScript desarrollada por Facebook
* [Redux] - contenedor predecible del estado de aplicaciones JavaScript.
* [NodeJS] - entorno JavaScript de lado de servidor que utiliza un modelo asíncrono y dirigido por eventos.
* [Express] - framework de nodejs.

### Paso 1 - 5 Puntos
* Clonar el Proyecto.
* Cambia a hacia la rama develop
* Clonar cambios de la rama develop a una nueva rama con tu nombre
* Subir rama nueva al respositorio con tus datos personales dentro del archivo readme.md
### Paso 2 - 50 Puntos
* Levantar el servidor utilizando expressjs
* Corregir los errores de la API ... [Recuerda utilizar ECMA Script]
* Crear una nueva tabla en mongo con la sigiente estructura: Incapacidades(folio:String(8),idEmpleados:numeric(4), adscripcion:String(100), TipoIncapacidad:String(100),  ramoSeguro:String(100), ProbableRiesgo:String(100), adscripcion:String(100), diasAcumulados:numeric(3))
* Crear una nueva API post que registre en la tabla anterior, teniendo en cuenta que no se podra registrar una incpacidad si el empleado es menos a 23 años.
* Crear una API post la cual regrese todos los empleados, en caso de recibir idEmpleado como parametro regresar solo el empleado que tenga asignado el valor del parametro idEmpleado, cuya respuesta tenga el siguiente formato:
```sh
{
headerResponse:{
    code:200,
    message:'Exito al consultar',
},
payload:{
    empleados:[{
    nombreEmpleado:"Genaro Ramos",
    fechaNacimiento:"1994-05-21",
    edad:"26 años",
    }
    ]
}    
}
```
### Paso 3 - 40 Puntos
* Generar proyecto nuevo con create React App con el nombre del solicitante
* Agregar Redux[Store, Actions, Reducers, Selectores, Reselect]
* Utilizar alguna dependencia de diseño.
* Todos los componentes creados deben ser responsive.
* Crear un Componente con un Select, el cual debera ser llenado con los datos obtenidos de la siguiente API ...
* Crear un componente con una tabla, debera ser llenada con los datos obtenidos de la siguiente API ... teniendo en cuenta que el parametro que se enviara sera el select anteriormente creado.
* Crear un formulario para la inserccion de Incapacidades utilizando la API ...
* La Actualizacion del select y la tabla deben ser en automatico.

### Paso 4 - 5 Puntos
* Hacer commit por cada cambio en su rama.
* Empujar cambios a su rama en el repositorio.
* Pasar cambios de su rama a QA.
* Resolver Conflictos en el merge
